extends KinematicBody2D

signal precio_colectado(tipo, vendedor, direccion)
signal actualizar_super_velocidad
signal actualizar_super_heroina
signal usar_super_heroina
signal congelar_enemigos
signal descongelar_enemigos

export var velocidad_jugador = 200
var ultima_direccion = Vector2(0, 1)

var direccion_objetivo = Vector2()
var velocidad = Vector2()

var vendedor_pulsado = null
var enemigo_pulsado = null
var direccion_opuesta = ""
var sonido_sh

func start(pos):
	position = pos
	show()

func _ready():
	var _sv = self.connect("actualizar_super_velocidad", get_parent(), "actualizar_super_velocidad")
	var _sh = self.connect("actualizar_super_heroina", get_parent(), "actualizar_super_heroina")
	var _ush = self.connect("usar_super_heroina", get_parent().get_node("HUD"), "mover_super_heroina")
	var _ce = self.connect("congelar_enemigos", get_parent(), "_on_congelar_enemigos")
	var _de = self.connect("descongelar_enemigos", get_parent(), "_on_descongelar_enemigos")
	var _tsv = $TimerSuperVelocidad.connect("timeout", self, "termina_super_velocidad")
	var _tsh = $TimerSuperHeroina.connect("timeout", self, "termina_super_heroina")
	sonido_sh = AudioStreamPlayer.new()
	self.add_child(sonido_sh)
	sonido_sh.stream = load("res://Sonidos/super_heroe_sonido.ogg")

func _on_congelar():
	set_process(false)
	set_process_input(false)
	set_process_internal(false)
	set_process_unhandled_input(false)
	set_process_unhandled_key_input(false)
	set_physics_process(false)
	set_physics_process_internal(false)

func _input(event):
	if event.is_action_pressed("lmb"):
		direccion_objetivo = get_global_mouse_position()
	if event.is_action_pressed("boton_super_velocidad"):
		usar_super_velocidad()
	if event.is_action_pressed("boton_super_heroina"):
		usar_super_heroina()

func _physics_process(_delta):
	var direccion: Vector2

	velocidad = position.direction_to(direccion_objetivo) * velocidad_jugador
	direccion = direccion_objetivo

	if position.distance_to(direccion_objetivo) > 5 and direccion_objetivo != Vector2.ZERO:
		velocidad = move_and_slide(velocidad)
		animar_jugador(direccion)
	else:
		animar_jugador(Vector2.ZERO)

	detectar_vendedor()

func _vendedor_pulsado(nombre):
	vendedor_pulsado = nombre

func animar_jugador(direccion: Vector2):
	if direccion != Vector2.ZERO:
		ultima_direccion = 0.5 * ultima_direccion + 0.5 * direccion
		var animacion = obtener_direccion_animacion(ultima_direccion) + "_caminar"
		$Sprite.play(animacion)
	else:
		var animacion = obtener_direccion_animacion(ultima_direccion) + "_quieto"
		$Sprite.play(animacion)

func detectar_vendedor():
	var objetos = $Area2D.get_overlapping_bodies()

	if objetos.size() > 1:
		var vendedor = null
		for o in objetos:
			if o.is_in_group("vendedores"):
				vendedor = o
				continue

		if vendedor != null and vendedor.is_in_group("vendedores"):
			if vendedor.is_in_group("vendedor_granos") and vendedor.name == vendedor_pulsado:
				vendedor_pulsado = null
				emit_signal("precio_colectado", "Granos", vendedor.name, "abajo")

			if vendedor.is_in_group("vendedor_hortalizas") and vendedor.name == vendedor_pulsado:
				vendedor_pulsado = null
				emit_signal("precio_colectado", "Hortalizas", vendedor.name, "abajo")

			if vendedor.is_in_group("vendedor_frutas") and vendedor.name == vendedor_pulsado:
				vendedor_pulsado = null
				emit_signal("precio_colectado", "Frutas", vendedor.name, "abajo")

func obtener_direccion_animacion(_direccion: Vector2):
	#var norm_direccion = direccion.normalized()
	var grados = rad2deg(velocidad.angle_to(Vector2(0,1)))

	if grados > -45 and grados <= 45:
		return "abajo"
	elif (grados < -135 and grados >= -180) or (grados > 135 and grados <=180):
		return "arriba"
	elif grados < -45 and grados >= -135:
		return "izquierda"
	elif grados > 45 and grados <= 135:
		return "derecha"
	return "abajo"

func usar_super_velocidad():
	if $TimerSuperVelocidad.is_stopped():
		if get_parent().cantidad_super_velocidad > 0:
			get_parent().cantidad_super_velocidad = get_parent().cantidad_super_velocidad - 1
			$TimerSuperVelocidad.start()
			velocidad_jugador = velocidad_jugador * 2
			$Aura.play()
			$Aura.show()
			emit_signal("actualizar_super_velocidad")
		
func termina_super_velocidad():
	$Aura.hide()
	$Aura.stop()
	velocidad_jugador = velocidad_jugador / 2

func usar_super_heroina():
	if $TimerSuperHeroina.is_stopped():
		if get_parent().cantidad_super_heroina > 0:
			get_parent().cantidad_super_heroina = get_parent().cantidad_super_heroina - 1
			$TimerSuperHeroina.start()
			emit_signal("actualizar_super_heroina")
			emit_signal("usar_super_heroina")
			emit_signal("congelar_enemigos")
			sonido_sh.play()
		
func termina_super_heroina():
	emit_signal("descongelar_enemigos")

func new_game():
	var grupos = {"Granos": 0, "Frutas": 0}
	start($InicioPosicion.position)
	$StartTimer.start()
	print(grupos)
