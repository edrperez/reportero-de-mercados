extends CanvasLayer

signal start_game
signal nivel_terminado
signal boton_super_velocidad
signal boton_super_heroina
signal boton_reiniciar_juego

var precios_solicitados
var precios_colectados
var super_heroina_posicion_original

func _ready():
	var _bsv = self.connect("boton_super_velocidad", get_parent().get_node("Jugador"), "usar_super_velocidad")
	var _bsh = self.connect("boton_super_heroina", get_parent().get_node("Jugador"), "usar_super_heroina")
	var _brj = self.connect("boton_reiniciar_juego", get_parent(), "_on_reiniciar_juego")
	super_heroina_posicion_original = $SuperHeroinaVolando.position
	
func mover_super_heroina():
	var inicio = super_heroina_posicion_original
	var final = Vector2(2120, 48)
	var tween = $Tween
	tween.playback_speed = 0.1
	tween.interpolate_property($SuperHeroinaVolando, "position",
			inicio, final, 1,
			Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	
func _on_precios_datos(colectados, solicitados):
	precios_colectados = colectados
	precios_solicitados = solicitados

func show_message(text):
	$MensajeLabel.text = text
	$MensajeLabel.show()
	$MensajeTimer.start()

func show_game_over():
	show_message("Game Over")
	
	yield($MensajeTimer, "timeout")
	
	$MensajeLabel.text = "Reportero de Mercados"
	$MensajeLabel.show()
	
	yield(get_tree().create_timer(1), "timeout")
	
	$IniciarButton.show()

func _on_actualizar_super_velocidad(cantidad):
	$SuperVelocidadDisponible.text = "{0}".format([cantidad])

func _on_actualizar_super_heroina(cantidad):
	$SuperHeroinaDisponible.text = "{0}".format([cantidad])

func actualizar_punteo(tipo, cantidad):
	var texto = ""
	var colectados = 0
	var por_colectar = precios_colectados.size()
	var sumado = false
	
	if tipo.empty() == false:
		if precios_colectados[tipo] != precios_solicitados[tipo]:
			precios_colectados[tipo] += cantidad
			sumado = true

	for t in precios_solicitados:
		if precios_colectados[t] == precios_solicitados[t]:
			colectados = colectados + 1
		texto += "{0}: {1}/{2} ".format([t, precios_colectados[t], precios_solicitados[t]])

	$BarraSuperiorLabel.text = texto
	if colectados == por_colectar:
		emit_signal("nivel_terminado")

	return sumado

func _on_nivel_terminado():
	$MensajeLabel.text = "¡Has colectado todos los precios!"

func _on_MessageTimer_timeout():
	$MensajeLabel.hide()

func _on_StartButton_pressed():
	$IniciarButton.hide()
	emit_signal("start_game")
	get_parent().get_node("Jugador").show()
	get_parent().get_node("Jugador").set_process(true)
	get_parent().get_node("Jugador").set_process_input(true)
	get_parent().get_node("Jugador").set_process_internal(true)
	get_parent().get_node("Jugador").set_process_unhandled_input(true)
	get_parent().get_node("Jugador").set_process_unhandled_key_input(true)
	get_parent().get_node("Jugador").set_physics_process(true)
	get_parent().get_node("Jugador").set_physics_process_internal(true)
	
	#get_parent().get_node("Jugador").start(get_parent().get_node("InicioPosicion").position)
	#get_parent().get_node("InicioTimerGranos").start()


func _on_BotonSuperVelocidad_pressed():
	emit_signal("boton_super_velocidad")


func _on_BotonSuperHeroina_pressed():
	emit_signal("boton_super_heroina")


func _on_BotonReiniciarJuego_pressed():
	emit_signal("boton_reiniciar_juego")
