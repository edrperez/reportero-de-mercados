extends Node

var score = 0
var nuevo_grano = true
var nueva_hortaliza = true
var nueva_fruta = true
var sonido_precio_colectado
var precios_solicitados = {"Granos": 1, "Frutas": 1, "Hortalizas": 1}
var precios_colectados = {"Granos": 0, "Frutas": 0, "Hortalizas": 0}
var precios_super_velocidad = 0
var precios_super_velocidad_necesarios = 5
var cantidad_super_velocidad = 0
var ko_super_heroina = 0
var ko_super_heroina_necesarios = 5
var cantidad_super_heroina = 0
var super_heroina_en_uso = false

const escena_enemigo_1 = preload("res://Entidades/Enemigos/1/enemigo_1.tscn") 

signal precios_datos(colectados_solicitados)
signal congelar_jugador
signal mensaje_final
signal actualizar_super_velocidad(cantidad_super_velocidad)
signal actualizar_super_heroina(cantidad_super_heroina)

func game_over():
	$PunteoTimer.stop()
	#$MobTimer.stop()
	$HUD.show_game_over()

func new_game():
	score = 0
	$Jugador.start($Posicion_Inicial_Jugador.position)
	$InicioTimerGranos.start()
	$HUD.actualizar_punteo(score)
	$HUD.show_message("Preparate")

func _on_precio_colectado(tipo, vendedor, direccion):
	var sumado = false
	if tipo == "Granos" and nuevo_grano == true:
		sumado = $HUD.actualizar_punteo(tipo, 1)
		nuevo_grano = false
		$InicioTimerGranos.start()

	if tipo == "Hortalizas" and nueva_hortaliza == true:
		sumado = $HUD.actualizar_punteo(tipo, 1)
		nueva_hortaliza = false
		$InicioTimerHortalizas.start()

	if tipo == "Frutas" and nueva_fruta == true:
		sumado = $HUD.actualizar_punteo(tipo, 1)
		nueva_fruta = false
		$InicioTimerFrutas.start()

	if sumado:
		get_node(vendedor).get_node("Sprite").play("{0}_quieto".format([direccion]))
		for h in get_tree().get_nodes_in_group("vendedor_{0}".format([tipo.to_lower()])):
			h.get_node("Icono").hide()
		sonido_precio_colectado.play()
		precios_super_velocidad = precios_super_velocidad + 1
		otorgar_super_velocidad()


func _on_puede_nuevo_grano():
	nuevo_grano = true
	for vendedor in get_tree().get_nodes_in_group("vendedor_granos"):
		vendedor.get_node("Icono").show()

func _on_puede_nueva_hortaliza():
	nueva_hortaliza = true
	for vendedor in get_tree().get_nodes_in_group("vendedor_hortalizas"):
		vendedor.get_node("Icono").show()

func _on_puede_nueva_fruta():
	nueva_fruta = true
	for vendedor in get_tree().get_nodes_in_group("vendedor_frutas"):
		vendedor.get_node("Icono").show()

func _on_nivel_terminado():
	emit_signal("mensaje_final")
	emit_signal("congelar_jugador")
	_on_congelar_enemigos()
	$MusicaFondo.stop()
	$MusicaFondo.stream = load("res://Sonidos/transicion.ogg")
	$MusicaFondo.play()
	$HUD/BotonReiniciarJuego.show()

func _on_precio_robado():
	$HUD.actualizar_punteo("", 0)

func _on_reiniciar_juego():
	var _rj = get_tree().reload_current_scene()

func _ready():
	randomize()
	precios_solicitados = {
		"Granos": range(1,8)[randi()%range(1,8).size()],
		"Frutas": range(1,8)[randi()%range(1,8).size()],
		"Hortalizas": range(1,8)[randi()%range(1,8).size()]}
	var _pc = $Jugador.connect("precio_colectado", self, "_on_precio_colectado")
	var _nt = $HUD.connect("nivel_terminado", self, "_on_nivel_terminado")
	var _mf = self.connect("mensaje_final", $HUD, "_on_nivel_terminado")
	var _pd = self.connect("precios_datos", $HUD, "_on_precios_datos")
	var _cj = self.connect("congelar_jugador", $Jugador, "_on_congelar")
	var _sv = self.connect("actualizar_super_velocidad", $HUD, "_on_actualizar_super_velocidad")
	var _sh = self.connect("actualizar_super_heroina", $HUD, "_on_actualizar_super_heroina")
	emit_signal("precios_datos", precios_colectados, precios_solicitados)
	emit_signal("actualizar_super_velocidad", cantidad_super_velocidad)
	emit_signal("actualizar_super_heroina", cantidad_super_heroina)
	$Jugador.start($Posicion_Inicial_Jugador.position)

	$HUD.actualizar_punteo("", 0)
	$InicioTimerGranos.stop()
	$InicioTimerHortalizas.stop()
	$InicioTimerFrutas.stop()

	var _itg = $InicioTimerGranos.connect("timeout", self, "_on_puede_nuevo_grano")
	var _ith = $InicioTimerHortalizas.connect("timeout", self, "_on_puede_nueva_hortaliza")
	var _itf = $InicioTimerFrutas.connect("timeout", self, "_on_puede_nueva_fruta")
	
	for h in self.get_children():
		if h is Position2D and h.is_in_group("creador_enemigos_1"):
			h.get_node("Timer").start(0.01)
	
	sonido_precio_colectado = AudioStreamPlayer.new()
	self.add_child(sonido_precio_colectado)
	#https://freesound.org/people/bradwesson/sounds/135936/
	sonido_precio_colectado.stream = load("res://Sonidos/precio_colectado.wav")
	
func _on_congelar_enemigos():
	super_heroina_en_uso = true
	for h in self.get_children():
		if h is KinematicBody2D and h.is_in_group("enemigos"):
			h._on_congelar()

func _on_descongelar_enemigos():
	super_heroina_en_uso = false
	for h in self.get_children():
		if h is KinematicBody2D and h.is_in_group("enemigos"):
			h._on_descongelar()

func _enemigo_pulsado(enemigo):
	ko_super_heroina = ko_super_heroina + 1
	otorgar_super_heroina()
	for g in enemigo.get_groups():
		if self.has_node(g):
			get_node(g).get_node("Timer").start()
			continue

func actualizar_super_heroina():
	emit_signal("actualizar_super_heroina", cantidad_super_heroina)

func otorgar_super_heroina():
	if cantidad_super_heroina < 99:
		if ko_super_heroina >= ko_super_heroina_necesarios:
			cantidad_super_heroina = cantidad_super_heroina + 1
			ko_super_heroina = 0
			actualizar_super_heroina()

func actualizar_super_velocidad():
	emit_signal("actualizar_super_velocidad", cantidad_super_velocidad)

func otorgar_super_velocidad():
	if cantidad_super_velocidad < 99:
		if precios_super_velocidad >= precios_super_velocidad_necesarios:
			cantidad_super_velocidad = cantidad_super_velocidad + 1
			precios_super_velocidad = 0
			actualizar_super_velocidad()
