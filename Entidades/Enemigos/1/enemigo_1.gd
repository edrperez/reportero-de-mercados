extends KinematicBody2D

signal puede_moverse
signal enemigo_pulsado
signal precio_robado

var velocidad = 50
var x = 0
var y = 0
var se_puede_mover = false
var ultima_direccion = Vector2(0, 1)
var direccion
var posicion_original
var x_limite = 256
var y_limite = 256
var x_limite_mayor
var x_limite_menor
var y_limite_mayor
var y_limite_menor
var vision
var angulo
var deteccion = 200
var fov = 80
const color_rojo = Color(1.0, 0, 0, 0.2)
const color_verde = Color(0, 1.0, 0, 0.2)
var color_cono = color_verde
var persiguiendo = false
var sonar_alarma = false
var sonido_alarma
var sonido_ko
var sonido_precio_robado
var direccion_enemigo = "derecha"
var estado = "patrulla"
var viajando = Vector2.ZERO
var jugador
var velocidad_animacion
var animacion
var puede_perseguir = true
var ko = false
var tex_diablito = load("res://Entidades/Burbujas/burbuja_diablito_feliz.png")
var tex_durmiendo = load("res://Entidades/Burbujas/burbuja_durmiendo.png")
var tex_llorando = load("res://Entidades/Burbujas/burbuja_llorando.png")
var tex_exclamacion = load("res://Entidades/Burbujas/burbuja_exclamacion.png")
var tex_asustado = load("res://Entidades/Burbujas/burbuja_asustado.png")
var tex_ko = load("res://Entidades/Burbujas/burbuja_ko.png")

func _ready():
	velocidad_animacion = $Sprite.frames.get_animation_speed("abajo_caminar")
	jugador = get_parent().get_node("Jugador")
	$Icono.hide()
	var altura_sprite = $Sprite.frames.get_frame("abajo_quieto", 0).get_height()
	var _ep = self.connect("enemigo_pulsado", get_parent(), "_enemigo_pulsado")
	var _pr = self.connect("precio_robado", get_parent(), "_on_precio_robado")
	var _tp = $TimerPersecucion.connect("timeout", self, "_on_detener_persecucion")
	var _tpp = $TimerPuedePerseguir.connect("timeout", self, "_on_puede_perseguir")
	randomize()

	posicion_original = self.position
	x_limite_mayor = posicion_original.x + x_limite
	x_limite_menor = posicion_original.x - x_limite
	y_limite_mayor = posicion_original.y + y_limite - altura_sprite / 2
	y_limite_menor = posicion_original.y - y_limite + altura_sprite / 2

	var _pm = self.connect("puede_moverse", self, "_puede_moverse")
	emit_signal("puede_moverse")
	
	sonido_alarma = AudioStreamPlayer.new()
	self.add_child(sonido_alarma)
	#https://www.tunepocket.com/royalty-free-music/8-bit-death-destruction-sound-effects/
	sonido_alarma.stream = load("res://Sonidos/alerta_enemigo.wav")
	
	sonido_precio_robado = AudioStreamPlayer.new()
	self.add_child(sonido_precio_robado)
	#https://freesound.org/people/sonsdebarcelona/sounds/187803/
	sonido_precio_robado.stream = load("res://Sonidos/precio_robado.wav")
	
	sonido_ko = AudioStreamPlayer.new()
	self.add_child(sonido_ko)
	#https://freesound.org/people/ccolbert70Eagles23/sounds/423526/
	sonido_ko.stream = load("res://Sonidos/sonido_ko.ogg")
	
	vision = definir_conos("abajo")

func _on_congelar():
	if jugador_atras():
		$Icono.texture = tex_ko
	else:
		$Icono.texture = tex_asustado
	$Icono.show()
	set_process(false)
	set_process_input(false)
	set_process_internal(false)
	set_process_unhandled_input(false)
	set_process_unhandled_key_input(false)
	set_physics_process(false)
	set_physics_process_internal(false)

func _on_descongelar():
	$Icono.texture = tex_exclamacion
	$Icono.hide()
	set_process(true)
	set_process_input(true)
	set_process_internal(true)
	set_process_unhandled_input(true)
	set_process_unhandled_key_input(true)
	set_physics_process(true)
	set_physics_process_internal(true)

func _physics_process(delta):
	if $Area2D.overlaps_body(jugador):
		if puede_perseguir:
			if robar_precio():
				sonido_precio_robado.play()
				$Icono.texture = tex_diablito
				puede_perseguir = false
				estado = "volver_base"
				$TimerPuedePerseguir.start()
				$TimerPersecucion.stop()
		
	if estado == "patrulla":
		if se_puede_mover == true:
			viajando = velocidad * direccion * delta
			viajando = move_and_slide(viajando)
			animar_enemigo(direccion)

	if estado == "persiguiendo":
		viajando = self.position.direction_to(jugador.position) * velocidad * 2
		viajando = move_and_slide(viajando)
		direccion = self.position.direction_to(jugador.position)
		animar_enemigo(direccion)

	if estado == "volver_base":
		if self.position.distance_to(posicion_original) > 50:
			viajando = position.direction_to(posicion_original) * velocidad * 2
			viajando = move_and_slide(viajando)
			direccion = self.position.direction_to(posicion_original)
			animar_enemigo(direccion)
		else:
			if puede_perseguir:
				$Icono.texture = tex_exclamacion
				$Icono.hide()
				estado = "patrulla"

	angulo = 90 - rad2deg(vision.angle())
	var detectado = false
	
	if jugador_tiene_precios() and puede_perseguir and self.position.distance_to(jugador.position) < deteccion:
		var direccion_jugador = self.position.direction_to(jugador.position)
		var angulo_al_jugador = rad2deg(vision.angle_to(direccion_jugador))

		if abs(angulo_al_jugador) < fov/2:
			if !sonar_alarma:
				sonar_alarma = true
				sonido_alarma.play()
			detectado = true
			estado = "persiguiendo"
			$Icono.texture = tex_exclamacion
			$Icono.show()
			$TimerPersecucion.start()
	
	if detectado:
		color_cono = color_rojo
	else:
		color_cono = color_verde
	update()

func _on_puede_perseguir():
	puede_perseguir = true
	$Icono.hide()

func _on_detener_persecucion():
	sonar_alarma = false
	estado = "volver_base"
	puede_perseguir = true
	$TimerPuedePerseguir.stop()
	$Icono.texture = tex_llorando

func _draw():
	if puede_perseguir:
		var centro = Vector2(0, 0)
		var radio = deteccion
		var angulo_desde = 180 + angulo - fov/2
		var angulo_hasta = 180 + angulo + fov/2
		
		if direccion_enemigo == "derecha" or direccion_enemigo == "izquierda":
			angulo_desde =  180 - angulo - fov/2
			angulo_hasta =  180 - angulo + fov/2
	
		var color = color_cono
		draw_circle_arc_poly(centro, radio, angulo_desde, angulo_hasta, color)

#https://docs.godotengine.org/en/3.2/tutorials/2d/custom_drawing_in_2d.html
func draw_circle_arc_poly(center, radius, angle_from, angle_to, color):
	var nb_points = 32
	var points_arc = PoolVector2Array()
	points_arc.push_back(center)
	var colors = PoolColorArray([color])

	for i in range(nb_points + 1):
		var angle_point = deg2rad(angle_from + i * (angle_to - angle_from) / nb_points - 90)
		points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)
	draw_polygon(points_arc, colors)

func jugador_atras():
	if $RayCast2D1.is_colliding() and $RayCast2D1.get_collider().name == "Jugador":
		return true
	if $RayCast2D2.is_colliding() and $RayCast2D2.get_collider().name == "Jugador":
		return true
	if $RayCast2D3.is_colliding() and $RayCast2D3.get_collider().name == "Jugador":
		return true

func jugador_tiene_precios():
	var total = 0
	for t in get_parent().precios_colectados:
		total = total + get_parent().precios_colectados[t]

	if total > 0:
		return true
	else:
		return false

func robar_precio(cantidad = 1):
	var limite = get_parent().precios_colectados.size()
	var tipo = range(0,limite)[randi()%range(0,limite).size()]

	var i = 0
	for t in get_parent().precios_colectados:
		if (i == tipo):
			if get_parent().precios_colectados[t] > 0:
				get_parent().precios_colectados[t] = get_parent().precios_colectados[t] - cantidad
				emit_signal("precio_robado")
				return true
		i = i + 1

func definir_conos(direccion_cono):
	if direccion_cono == "arriba":
		return self.position.direction_to(Vector2(self.position.x, self.position.y - deteccion))
	elif direccion_cono == "abajo":
		return self.position.direction_to(Vector2(self.position.x, self.position.y + deteccion))
	elif direccion_cono == "izquierda":
		return self.position.direction_to(Vector2(self.position.x - deteccion, self.position.y))
	elif direccion_cono == "derecha":
		return self.position.direction_to(Vector2(self.position.x + deteccion, self.position.y))

func animar_enemigo(direccion_animacion: Vector2):
	if direccion_animacion != Vector2.ZERO:
		ultima_direccion = 0.5 * ultima_direccion + 0.5 * direccion_animacion
		
		animacion = obtener_direccion_animacion(ultima_direccion)
		vision = definir_conos(direccion_enemigo)
		
		if estado == "persiguiendo":
			$Sprite.frames.set_animation_speed(animacion + "_caminar", velocidad_animacion * 2)
		else:
			$Sprite.frames.set_animation_speed(animacion + "_caminar", velocidad_animacion)
		$Sprite.play(animacion + "_caminar")

func obtener_direccion_animacion(direccion_animacion: Vector2):
	var norm_direccion = direccion_animacion.normalized()
	if norm_direccion.y >= 0.707:
		direccion_enemigo = "abajo"
		ajustar_raycasts("arriba")
		$Area2D/CollisionShape2DAA.position.x = -1
		$Area2D/CollisionShape2DAA.position.y = 37
		return "abajo"
	elif norm_direccion.y <= -0.707:
		ajustar_raycasts("abajo")
		$Area2D/CollisionShape2DAA.position.x = -1
		$Area2D/CollisionShape2DAA.position.y = -37
		direccion_enemigo = "arriba"
		return "arriba"
	elif norm_direccion.x <= -0.707:
		ajustar_raycasts("derecha")
		$Area2D/CollisionShape2DID.position.x = -18
		$Area2D/CollisionShape2DID.position.y = 0
		direccion_enemigo = "izquierda"
		return "izquierda"
	elif norm_direccion.x >= 0.707:
		ajustar_raycasts("izquierda")
		$Area2D/CollisionShape2DID.position.x = 17
		$Area2D/CollisionShape2DID.position.y = 0
		direccion_enemigo = "derecha"
		return "derecha"
	$Area2D/CollisionShape2DAA.position = Vector2(-1, 35)
	return "abajo"

func ajustar_raycasts(contrario):
	if contrario == "derecha":
		$RayCast2D1.position.x = 13
		$RayCast2D1.position.y = -30
		$RayCast2D2.position.x = 13
		$RayCast2D2.position.y = 0
		$RayCast2D3.position.x = 13
		$RayCast2D3.position.y = 30
		$RayCast2D1.cast_to = Vector2(40, 0)
		$RayCast2D2.cast_to = Vector2(40, 0)
		$RayCast2D3.cast_to = Vector2(40, 0)

	if contrario == "izquierda":
		$RayCast2D1.position.x = -13
		$RayCast2D1.position.y = -30
		$RayCast2D2.position.x = -13
		$RayCast2D2.position.y = 0
		$RayCast2D3.position.x = -13
		$RayCast2D3.position.y = 30
		$RayCast2D1.cast_to = Vector2(-40, 0)
		$RayCast2D2.cast_to = Vector2(-40, 0)
		$RayCast2D3.cast_to = Vector2(-40, 0)

	if contrario == "arriba":
		$RayCast2D1.position.x = -13
		$RayCast2D1.position.y = -34
		$RayCast2D2.position.x = 0
		$RayCast2D2.position.y = -34
		$RayCast2D3.position.x = 13
		$RayCast2D3.position.y = -34
		$RayCast2D1.cast_to = Vector2(0, -40)
		$RayCast2D2.cast_to = Vector2(0, -40)
		$RayCast2D3.cast_to = Vector2(0, -40)

	if contrario == "abajo":
		$RayCast2D1.position.x = -13
		$RayCast2D1.position.y = 34
		$RayCast2D2.position.x = 0
		$RayCast2D2.position.y = 34
		$RayCast2D3.position.x = 13
		$RayCast2D3.position.y = 34
		$RayCast2D1.cast_to = Vector2(0, 40)
		$RayCast2D2.cast_to = Vector2(0, 40)
		$RayCast2D3.cast_to = Vector2(0, 40)

func _puede_moverse():
	var xx1 = x_limite_menor
	var xx2 = x_limite_mayor
	var yy1 = y_limite_menor
	var yy2 = y_limite_mayor

	if xx1 == 0 and xx2 == 0:
		x = 0
	else:
		x = range(xx1,xx2)[randi()%range(xx1,xx2).size()]

	if yy1 == 0 and yy2 == 0:
		y = 0
	else:
		y = range(yy1,yy2)[randi()%range(yy1,yy2).size()]
	
	var destino = Vector2(x, y)

	var diferencia = (destino - self.position).normalized() * velocidad

	direccion = diferencia
	se_puede_mover = true
	yield(get_tree().create_timer(self.position.distance_to(destino)/velocidad), "timeout")
	animacion = obtener_direccion_animacion(ultima_direccion)
	$Sprite.play(animacion + "_quieto")
	timer_movimiento()

func timer_movimiento():
	se_puede_mover = false
	yield(get_tree().create_timer(4.0), "timeout")
	emit_signal("puede_moverse")

func parpadear_icono():
	yield(get_tree().create_timer(2.0 + randf()), "timeout")
	get_node("Icono").modulate = Color(1.0, 1.0, 1.0, 0.0)
	yield(get_tree().create_timer(2.0 + randf()), "timeout")
	get_node("Icono").modulate = Color(1.0, 1.0, 1.0, 1.0)
	parpadear_icono()

func _on_KinematicBody2D_input_event(_viewport, _event, _shape_idx):
	if Input.is_action_pressed("lmb"):
		#emit_signal("ENEMY_CLICKED", self, event.button_index)
		if jugador_atras() and ko == false and jugador_tiene_precios():
			ko = true
			sonido_ko.play()
			_on_congelar()
			yield(get_tree().create_timer(2.0), "timeout")
			emit_signal("enemigo_pulsado", self)
			self.queue_free()
