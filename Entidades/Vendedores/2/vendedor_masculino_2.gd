extends KinematicBody2D

signal puede_moverse
signal vendedor_pulsado

var velocidad = 50
var x = 0
var y = 0
var se_puede_mover = false
var ultima_direccion = Vector2(0, 1)
var direccion
var posicion_original
var x_limite = 128
var y_limite = 128
var x_limite_mayor
var x_limite_menor
var y_limite_mayor
var y_limite_menor

func _ready():
	$RayCast2DN.collide_with_bodies = false
	$RayCast2DS.collide_with_bodies = false
	$RayCast2DE.collide_with_bodies = false
	$RayCast2DO.collide_with_bodies = false
	var altura_sprite = $Sprite.frames.get_frame("abajo_quieto", 0).get_height()
	var _vp = self.connect("vendedor_pulsado", get_parent().get_node("Jugador"), "_vendedor_pulsado")
	randomize()

	posicion_original = self.position
	x_limite_mayor = posicion_original.x + x_limite
	x_limite_menor = posicion_original.x - x_limite
	y_limite_mayor = posicion_original.y + y_limite - altura_sprite / 2
	y_limite_menor = posicion_original.y - y_limite + altura_sprite / 2

	var _pm = self.connect("puede_moverse", self, "_puede_moverse")
	emit_signal("puede_moverse")

func _physics_process(delta):
	if se_puede_mover == true:
		var movimiento = velocidad * direccion * delta
		movimiento = move_and_slide(movimiento)
		animar_vendedor(direccion)

func obtener_raycast():
	var raycast_por_medir = obtener_direccion_animacion(ultima_direccion)
	
	if raycast_por_medir == "arriba":
		return $RayCast2DN

	if raycast_por_medir == "abajo":
		return $RayCast2DS

	if raycast_por_medir == "izquierda":
		return $RayCast2DE

	if raycast_por_medir == "derecha":
		return $RayCast2DO

	return false

func animar_vendedor(direccion_vendedor: Vector2):
	if direccion_vendedor != Vector2.ZERO:
		ultima_direccion = 0.5 * ultima_direccion + 0.5 * direccion_vendedor
		
		var animacion = obtener_direccion_animacion(ultima_direccion) + "_caminar"
		
		$Sprite.play(animacion)

func obtener_direccion_animacion(direccion_vendedor: Vector2):
	var norm_direccion = direccion_vendedor.normalized()
	if norm_direccion.y >= 0.707:
		return "abajo"
	elif norm_direccion.y <= -0.707:
		return "arriba"
	elif norm_direccion.x <= -0.707:
		return "izquierda"
	elif norm_direccion.x >= 0.707:
		return "derecha"
	return "abajo"

func _puede_moverse():
	var xx1 = x_limite_menor
	var xx2 = x_limite_mayor
	var yy1 = y_limite_menor
	var yy2 = y_limite_mayor

	if xx1 == 0 and xx2 == 0:
		x = 0
	else:
		x = range(xx1,xx2)[randi()%range(xx1,xx2).size()]

	if yy1 == 0 and yy2 == 0:
		y = 0
	else:
		y = range(yy1,yy2)[randi()%range(yy1,yy2).size()]
	
	var destino = Vector2(x, y)

	var diferencia = (destino - self.position).normalized() * velocidad

	direccion = diferencia
	se_puede_mover = true
	yield(get_tree().create_timer(self.position.distance_to(destino)/velocidad), "timeout")
	var animacion = obtener_direccion_animacion(ultima_direccion) + "_quieto"
	$Sprite.play(animacion)
	timer_movimiento()

func timer_movimiento():
	se_puede_mover = false
	yield(get_tree().create_timer(1.5), "timeout")
	emit_signal("puede_moverse")

func parpadear_icono():
	yield(get_tree().create_timer(2.0 + randf()), "timeout")
	get_node("Icono").modulate = Color(1.0, 1.0, 1.0, 0.0)
	#print("transparente")
	yield(get_tree().create_timer(2.0 + randf()), "timeout")
	get_node("Icono").modulate = Color(1.0, 1.0, 1.0, 1.0)
	#print("opaca")
	parpadear_icono()

func _on_KinematicBody2D_input_event(_viewport, _event, _shape_idx):
	if Input.is_action_pressed("lmb"):
		#emit_signal("ENEMY_CLICKED", self, event.button_index)
		emit_signal("vendedor_pulsado", self.name)
