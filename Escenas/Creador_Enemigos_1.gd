extends Position2D

const escena_enemigo_1 = preload("res://Entidades/Enemigos/1/enemigo_1.tscn") 

func _ready():
	var _tce = $Timer.connect("timeout", self, "_on_crear_enemigo")

func _on_crear_enemigo():
	var total_enemigos = 0
	for h in get_parent().get_children():
		if h is KinematicBody2D and h.is_in_group(self.name):
			total_enemigos = total_enemigos + 1

	if total_enemigos < 1:
		if !get_parent().super_heroina_en_uso:
			var enemigo_1 = escena_enemigo_1.instance()
			enemigo_1.add_to_group(self.name)
			enemigo_1.position = self.position
			enemigo_1.angulo = 90
			get_tree().get_current_scene().add_child(enemigo_1)
		$Timer.start(10)
